package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees);

        // change it so that it's a different company

        //testing and debugging a shallow copy
        System.out.println(employees[0]);
        System.out.println(employees[1]);
        System.out.println(employees[2]);

        employees[0].setEmployeeId(12);
        
        //second part of testing and debugging a shallow copy
        System.out.println(employees[0]);
        System.out.println(employees[1]);
        System.out.println(employees[2]);
        
        Company c2 = new Company(employees);
       
        
        assertNotEquals(c1, c2);
    }
}
